"Runs backend for dservice."

import sys
sys.path.append("./") # running from dservice.py
sys.path.append("../")

from flask import Flask, request

from dservicecommon import saveload
from dservicecommon import tables

app: Flask = Flask(__name__)


@app.get("/api/config/")
def get_config():
    "gets config"
    return saveload.read_machine_config()


@app.get("/api/config/<key>")
def get_specified_config(key):
    "gets specifies config item"
    return saveload.read_machine_config()[key]


@app.get("/api/table/")
def show_machine_table():
    "shows machine tables"
    return saveload.read_tables()


@app.get("/api/table/<key>")
def show_specified_machine_table(key):
    "shows specified machine key IP address"
    response = {"ip": format(tables.ip_from_machine_id(key))}
    return response


@app.post("/api/table/")
def add_machine_table(): # not implemented
    "adds machine table to existing table"
    req = request.get_json()
    print(req)

    existing = saveload.read_tables()
