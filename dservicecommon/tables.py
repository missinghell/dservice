"Module to provide tools for manipulation of mtable.yml"

from typing import Dict

import ipaddress
import requests
import json

from dservicecommon import saveload


def ip_from_machine_id(machine_id: str) -> ipaddress.IPv4Address or ipaddress.IPv6Address:
    "Returns IP address of machine from ID. Traverses machine ID tables."

    # Currently only returns direct IP match in local table.

    our_table: Dict[str, str] = saveload.read_tables()

    if machine_id in our_table.keys():
        if len(our_table[machine_id]) != 64:
            return ipaddress.ip_address(our_table[machine_id])
        else:
            req = requests.get(
                f"http://{format(ip_from_machine_id(our_table[machine_id]))}:5000/api/table/{machine_id}", timeout=5)
            return ipaddress.ip_address(req.json()["ip"])

    else:
        return None