"Includes functions to read and write to config."
from typing import Dict
import yaml


def read_tables(table_filename: str = "./config/mtable.yml") -> Dict[str, str]: # change to IP
    """
    Reads tables.

    Parameters
    ----------

    table_filename : str, default="./config/mtable.yml"
        Filename where table is stored

    Returns
    -------

    tables : Dict[str, str]
        Tables retrieved
    """
    with open(table_filename, "r", encoding="utf8") as table:
        tables = yaml.safe_load(table)

    return tables


def save_tables(tables: Dict[str,str],
        table_filename: str = "./config/mtable.yml") -> None: # change to IP
    """
    Saves tables to disk.

    Parameters
    ----------

    tables : Dict[str, str]
        Table (as dictionary) to store.
    
    table_filename : str, default="./config/machine.yml"
        Filename to store table.
    """
    with open(table_filename, "w", encoding="utf8") as table:
        yaml.safe_dump(tables, table)


def read_machine_config(machine_filename: str = "./config/machine.yml") -> Dict[str, str]:
    """
    Returns dictionary represenatation of machine data from yml file.

    Parameters
    ----------

    machine_filename : str, default="./config/machine.yml"
        Filename of where to read config. Defaults to ./config/machine.yml

    Returns
    -------

    machine_config : Dict[str, str]
        Dictionary containing key value pairs of machine config
    """
    with open(machine_filename, "r", encoding="utf8") as machine_file:
        machine_config: Dict[str, str] = yaml.safe_load(machine_file)

    return machine_config


def save_machine_config(machine_config: Dict[str, str],
        machine_filename: str = "./config/machine.yml") -> None:
    """
    Saves machine data to yml.

    Parameters
    ----------
    machine_config : Dict[str, str]
        Config to save.

    machine_filename : str, default="./config/machine.yml"
        Filename of where to store config. Defaults to ./config/machine.yml
    """
    with open(machine_filename, "w", encoding="utf8") as machine_file:
        yaml.safe_dump(machine_config, machine_file)
