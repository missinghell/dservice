"""
Front-end to dservice.
Contains functions to initialise a machine with an ID and begin listening process.
"""

from typing import Dict

import argparse
import random
import string
import subprocess
import ipaddress
import socket

from dservicecommon import saveload


def get_ip() -> ipaddress.IPv4Address or ipaddress.IPv6Address:
    "Gets main ip of host (hackish)"
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    ip_addr = s.getsockname()[0]
    s.close()

    return ipaddress.ip_address(ip_addr)


def init_table(machine_id: str, addr: ipaddress.IPv4Address
        or ipaddress.IPv6Address = get_ip()) -> None:
    "Initialises table with machine ID to IP address."
    saveload.save_tables({machine_id: format(addr)})


def gen_machine_id() -> str:
    "Generates machine ID."
    machine_id: str = "".join(
        random.choice(
        string.ascii_letters + string.digits)
        for _ in range(64))

    return machine_id


def bootstrap() -> Dict[str,str]:
    "Bootstrap machine with machine ID."
    machine_config: Dict[str, str] = {}
    machine_id: str = gen_machine_id()

    machine_config["machine_id"] = machine_id
    saveload.save_machine_config(machine_config)

    init_table(machine_config["machine_id"])

    return machine_config


def parse() -> Dict[str, str]:
    "Parses arguments."
    parser: argparse.ArgumentParser = argparse.ArgumentParser()
    parser.add_argument("action", help="Starts/stops listener process.", choices=["start", "stop"])
    parser.add_argument("--new", action="store_true", help="Bootstraps node with new machine-id and wipes tables.")

    return vars(parser.parse_args())


def main() -> None:
    "main entrypoint for manager"
    args: Dict[str, str] = parse()

    machine_config: Dict[str, str] = bootstrap() if args["new"] else saveload.read_machine_config()

    if args["action"] == "start":
        subprocess.Popen(["flask", "--app", "listener/main", "run", "--host=0.0.0.0"], stdout=subprocess.DEVNULL)
    else:
        subprocess.Popen(["killall", "flask"]) # DANGEROUS


if __name__ == "__main__":
    main()
